# Informations générale

## Projet

Création d'un portfolio avec le cms Wordpress


## Mise en place

- création d'un serveur local avec EASYPHP
- téléchargement du cms Wordpress
- création du repository avec Bitbucket: git clone https://hhendrik21@bitbucket.org/hhendrik21/hh_worpress.git
- clone du projet avec Sourcetree
- création d'une base de données sql avec PHPmyAdmin


## Wordpress

### Le Thème

- choix d'un thème gratuit dans l'onglet apparence, téléchargement et activation.
"l'expérience fut très frustrante car le choix est très important et certain thème offre une possibilité restreinte dans la création des différentes pages" "mon manque d'expèrience m'a contraint à choisir un thème basique (Twenty Nineteen) ne me permettant pas de rendre un travail à la hauteur de mon envie"

### Le site

#### Le Brief

        Vous configurez votre site : 
- son titre : votre_Prénom_NOM - Portfolio 
- son slogan : Apprenant SIMPLON Chambéry - Le_nom_de_votre_Team 
- les permaliens doivent contenir le nom de l’article 
Votre site doit contenir au minimum : 
- 1 page d’accueil “statique” 
- 1 page de listing automatique d’articles 
- 5 articles (lorem ipsum autorisé) avec une image d’entête unique pour chaque article et la possibilité de les partager sur les réseaux sociaux 
- 2 pages catégories qui doivent avoir au moins 1 article associé 
- 1 formulaire de contact 
- 1 page de mentions légales (générée avec SubDelirium) 
- 1 menu qui permet de naviguer sur toutes les pages du site 
- 1 fichier sitemap.xml 
Votre site doit être référençable par les moteurs de recherche. 


#### Réalisation

- Installation de différents plugins (extensions): classic editor, elementor, sassy social share, wpforms lite, XML Sitemap & Google News 
- "wordpress est intuitif, mais le premier thème installé (Hestia) m'a bridé et l'expèrience n'a pas été concluante".
