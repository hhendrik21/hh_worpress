<?php
/**
 * Block Patterns
 * 
 * @link https://developer.wordpress.org/reference/functions/register_block_pattern/
 * @link https://developer.wordpress.org/reference/functions/register_block_pattern_category/
 *
 * @package Teletype
 * @since   1.4.0
 */

/**
 * Register Block Pattern Category.
 */
if ( function_exists( 'register_block_pattern_category' ) ) {
	register_block_pattern_category(
		'teletype',
		array( 'label' => __( 'Teletype Patterns', 'teletype' ) )
	);
}

/**
 * Register Block Patterns.
 */
if ( function_exists( 'register_block_pattern' ) ) {

	/**
	 *
	 * Getwid blocks plugin
	 *
	 */
	if ( class_exists( 'Getwid\Getwid' ) ) {

		// Posts Grid
		register_block_pattern(
			'teletype/posts-grid',
			array(
				'title'         => esc_html__( 'Posts Grid', 'teletype' ),
				'categories'    => array( 'teletype' ),
				'viewportWidth' => 1024,
				'content'       => '<!-- wp:getwid/recent-posts {"showDate":true,"showContent":true,"showFeaturedImage":true,"postLayout":"grid","className":"teletype-pattern"} /-->',
			)
		);

	}	
}